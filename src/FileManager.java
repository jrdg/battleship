import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;

public class FileManager
{
	String nom;
	String path;
	boolean havePath;
	
	
	//constructeur appeler quand on veu cree  un fichier qui se situe a la source
	public FileManager(String nom)
	{
		this.havePath = false;
		this.nom = nom;
	}
	
	//constructeur quon appel quand on veu cree un fichier qui sera situer dans un folder
	public FileManager(String nom , String path)
	{
		this.nom = nom;
		this.path = path;
		this.havePath = true;
		this.path = path;
		
		if(!(new File(this.path).exists()))
			this.createFolder(this.path);
			
	}
	
	
	//methode appeler pour cree un folder
	public static boolean createFolder(String nom)
	{
		boolean f;
		if(f = new File(nom).mkdirs())
			return true;
		else
			return false;			
	}
	
	//lit le fichier
	public String readFile()
	{
		String str = "";
		FileReader fr;
		
		try 
		{
			if(this.havePath)
				fr = new FileReader(this.path+"/"+this.nom);
			else
				fr = new FileReader(this.nom);
			
			int i = 0 ;
			
			while((i = fr.read()) != -1)
				str += (char)i;
			
			return str;
		}
		catch (FileNotFoundException e) 
		{
			System.out.println("LE FOLDER N'EXISTE PAS TU DOIT AVANT DE CHOISIR LE CONSTRUCTEUR AVEC LE STRING PATH UTILISE LA METHODE STATIC createFolder(String nom)");
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	//ecrit a la fin du fichier
	public void writeFile(String msg)
	{	
		FileWriter fw;

		try
		{
			if(this.havePath)
				fw = new FileWriter(this.path+"/"+this.nom,true);
			else
				fw = new FileWriter(this.nom,true);
			
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			
			pw.println(msg);
			pw.flush();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("LE FOLDER N'EXISTE PAS TU DOIT AVANT DE CHOISIR LE CONSTRUCTEUR AVEC LE STRING PATH UTILISE LA METHODE STATIC createFolder(String nom)");
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	
	//reset le fichier
	public void resetFile()
	{
		FileWriter fw;
		
		try
		{
			if(this.havePath)
				fw = new FileWriter(path+"/"+this.nom,false);
			else
				fw = new FileWriter(this.nom,false);
			
			fw.write(this.readFile().trim());
			fw.flush();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("LE FOLDER N'EXISTE PAS TU DOIT AVANT DE CHOISIR LE CONSTRUCTEUR AVEC LE STRING PATH UTILISE LA METHODE STATIC createFolder(String nom)");
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
