import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MultiPanel extends JPanel implements KeyListener
{
	private Player p1;
	private Player p2;
	private JPanel pan1; //panel global
	private JPanel pan2; //panel qui contient les grille de joueur
	public pan21 pan21; //grille du joueur 1
	public pan22 pan22; //grille du joueur 2
	private JPanel pan3; //panel qui contient les bateau a drag n drop
	private StatsPanel sp; //panneau des statistisque tout en haut
	private ActionListener p1Action;
	private ActionListener p2Action;
	private BoatPickerPanel bp;
	private int[][] p1Case;
	private int[][] p2Case;
	private Bateau[] bateauTab;
	public Bateau[] bateauTabOwn;
	private boolean playerTurn;
	private boolean isStart;
	private ArrayList<Integer> botCasePlayed;
	private final int timeBeforeBotPlay = 10;
	private IA IA;
	public int pts;
	
	
	////////////tous ski rapport au socket et au multijoeur
	private Reseau reseau; // variable qui sert a savoir si ses un serveur ou un client
	private Server server; //instance de serveur pour le demarer
	private Client client; //instance de client pour le demarer
	private boolean havePlayed; // true si le joueur a jouer false sinon
	private Reseau turnToPlay; // = server si ses au serveur a jour client si ses au client.
	private int casePlayed;
	private boolean enemyIsReady;
	private boolean playerIsReady;
	
	private Bateau[] porteAvionPos = new Bateau[4];
	
	
	public MultiPanel(Reseau reseau)
	{
		this.pts = 0;
		this.turnToPlay = Reseau.Server;
		this.playerIsReady = false;
		this.enemyIsReady = false;
		this.havePlayed = false;
		this.reseau = reseau;
		this.addKeyListener(this);
		IA = new IA();
		this.setFocusable(true);
		this.botCasePlayed = new ArrayList();
		p1= new Player("Player 1");
		p2 = new Player("Player 2");
		this.isStart = false;
		this.playerTurn = true;
		bateauTab = new Bateau[4];
		bateauTabOwn = new Bateau[4];
		p1Case = new int[10][10];
		p2Case = new int[10][10];
		bp = new BoatPickerPanel();
		this.setLayout(new BorderLayout());
		pan1 = new JPanel();
		pan3 = new JPanel();
		pan1.setLayout(new BorderLayout());
		sp = new StatsPanel();
		sp.setPlayerTurn(true);
		pan1.add(sp,BorderLayout.NORTH);
		pan2 = new JPanel();
		pan2.setLayout(new GridLayout(1,2));
		pan21 = new pan21();
		pan21.setBackground(Color.black);
		pan21.setLayout(new GridLayout(10,10));
		pan21.setBorder(BorderFactory.createEmptyBorder(0, 15, 15, 13));
		sp.setMulti(true);
		
		if(this.reseau == Reseau.Server)
			sp.setPlayerTurn(true);
		else
			sp.setPlayerTurn(false);
		
		sp.repaint();
		
		this.bp.getRandom().addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{			
				randomizePositioningPlayer();
				((JButton)arg0.getSource()).setEnabled(false);
			}
			
		});
		
		this.bp.getReady().addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				if(!bp.getRandom().isEnabled())
				{
					sp.setPlayer1Ready(true);
					System.out.println(sp.isPlayer1Ready());
					sp.repaint();
				}
				else
					JOptionPane.showMessageDialog(null, "You need to put all of your boat on the map","error",JOptionPane.ERROR_MESSAGE);
			}
			
		});
		
		this.checkReady();
		
		this.bp.getReset().addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(!isStart)
				{
					bateauTabOwn = new Bateau[4];
					
					for(int i = 0 ; i <= p1Case.length-1 ; i++)
					{
						for(int j = 0 ; j <= p1Case.length-1 ; j++)
							p1Case[i][j] = 0;
					}
					
					for(int i = 0 ; i <= 100-1 ; i++)
						pan21.getComponent(i).setBackground(Color.darkGray);
					
					bp.getRandom().setEnabled(true);
				}			
			}
			
		});
		
		System.out.println(reseau);
		
		p1Action = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				/*
				 * si ses ton tour de jour (la parti commence par le serveur)
				 */
				if(turnToPlay == reseau)
				{
				    int n = Integer.parseInt(((JButton)e.getSource()).getActionCommand());
				    casePlayed = n;
					havePlayed = true;
				}

			}		
		};
		
		
		for(int i = 0 ; i <= 100-1 ; i++)
		{
			pan21.add(new JButton());
			pan21.getComponent(i).setBackground(Color.DARK_GRAY);
			((JComponent) pan21.getComponent(i)).setBorder(new LineBorder(Color.white));
		}
			
		
		pan22 = new pan22();
		pan22.setBackground(Color.black);
		pan22.setLayout(new GridLayout(10,10));
		pan22.setBorder(BorderFactory.createEmptyBorder(0, 13, 15, 15));
		
		for(int i = 0 ; i <= 100-1 ; i++)
		{
			pan22.add(new JButton());
			((JButton)pan22.getComponent(i)).setActionCommand(Integer.toString(i));
			((JButton)pan22.getComponent(i)).addActionListener(p1Action);
			pan22.getComponent(i).setBackground(Color.DARK_GRAY);
			((JComponent) pan22.getComponent(i)).setBorder(new LineBorder(Color.white));
		}
			
	
		pan2.add(pan21);
		pan2.add(pan22);
		pan1.add(pan2);
		pan1.setBackground(Color.black);
		pan1.add(bp,BorderLayout.SOUTH);
		pan1.setPreferredSize(new Dimension(0,0));
		this.add(pan1);
		this.randomizePositioningBot();
		
		for(int i = 0 ; i <= pan21.getComponentCount()-1 ;i++)
		{
			pan21.getComponent(i).setEnabled(false);
		}
		
		 if(this.reseau == Reseau.Server)
			this.startServer();
		else
			this.startClient();
	}
	
	public StatsPanel getSp()
	{
		return this.sp;
	}
	
	public boolean getIsStart()
	{
		return this.isStart;
	}
	
	public void resetGame()
	{		
		this.IA.IAreset();
		
		//on reset tous le log
		this.bp.getLogPanel().resetLogOperation();
		
		//reset les tableau de bateau
		bateauTab = new Bateau[4];
		bateauTabOwn = new Bateau[4];
		
		//reset les tableau multidiemensionell
		p1Case = new int[10][10];
		p2Case = new int[10][10];
		
		//reset le temps
		sp.getMinObject().stop();
		sp.getMinObject().resetTime();
		
		//remet tous les bouton des 2 grille a enabled
		for(int i = 0 ; i <= 100-1 ; i++)
		{
			pan22.getComponent(i).setEnabled(true);
			pan21.getComponent(i).setEnabled(true);
		}
		
		
		//remet la couleurd e tous les boutons a darkgray
		for(int i = 0 ; i <= 100-1 ; i++)
		{
			pan21.getComponent(i).setBackground(Color.darkGray);
			pan22.getComponent(i).setBackground(Color.darkGray);
		}
		
		//remet le score a 0
		p1.setScore(0);
		p1.setOwnShipDestroyed(0);
		p1.setShipDestroyed(0);
		p2.setScore(0);
		p2.setOwnShipDestroyed(0);
		p2.setShipDestroyed(0);							
		sp.setScoreP1(p1.getScore());
		sp.setShipKilledP1(p1.getShipDestroyed());
		sp.setOwnShipDieP1(p1.getOwnShipDestroyed());
		sp.setScoreP2(p2.getScore());
		sp.setShipKilledP2(p2.getShipDestroyed());
		sp.setOwnShipDieP2(p2.getOwnShipDestroyed());
		
		sp.setTime(-1);
		
		sp.repaint();
		
		//remet la parti a pas start
		isStart = false;
		
		//remove tous les case deja jouer par lee bot
		this.botCasePlayed.clear();
		
		//renabled tous les bouton de demarrage
		bp.getRandom().setEnabled(true);
		bp.getReady().setEnabled(true);
		bp.getReset().setEnabled(true);
		
		//on genere des case aleatoire pour le bot
		randomizePositioningBot();	
	}
	
	public void throwStartTimer()
	{
		Thread tstart = new Thread(new Runnable()
		{
			@Override
			public void run() 
			{
				int timer = 3;
				
				while(timer > -1)
				{		
					if(timer == 0)
					{
						isStart = true;
						sp.getMinObject().startMinuterie(sp,Chrono.Up);
					}
						
					
					sp.setTime(timer);
					timer--;
					
					sp.repaint();
					synchronized(this)
					{	
						try {
							this.wait(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}		
		});
		
		tstart.start();
	}
	
	public void randomizePositioningBot()
	{
		boolean isEmpty = true;
		//Bateau b;
		Direction direction;
		
		
		for(int k = 0 ; k <= 4-1 ; k++)
		{
			do
			{
				isEmpty = true;
				int x = (int) Math.round(Math.random() * 9);
				int y = (int) Math.round(Math.random() * 9);
				int alignement = (int) Math.round(Math.random() * 1);
				
				if(alignement == 0)
					direction = Direction.Vertical;
				else
					direction = Direction.Horizontal;
				
				if(k == 0)		
					bateauTab[0] = new Destroyer(3,direction,x,y);
				else if (k == 1)
					bateauTab[1] = new PorteAvion(4,direction,x,y);
				else if(k == 2)
					bateauTab[2] = new SousMarin(3,direction,x,y);
				else
					bateauTab[3] = new BateauPatrouille(2,direction,x,y);
				
				
				
				if(bateauTab[k].direction == Direction.Vertical)
				{
					if(bateauTab[k].getNbCase() == 2)
					{
						if(bateauTab[k].getY() >= 9)
							isEmpty = false;
					}
					else if(bateauTab[k].getNbCase() == 3)
					{
						if(bateauTab[k].getY() >= 8)
							isEmpty = false;
					}
					else if(bateauTab[k].getNbCase() == 4)
					{
						if(bateauTab[k].getY() >= 7)
							isEmpty = false;
					}
				}
				else
				{
					if(bateauTab[k].getNbCase() == 2)
					{
						if(bateauTab[k].getX() >= 9)
							isEmpty = false;
					}
					else if(bateauTab[k].getNbCase() == 3)
					{
						if(bateauTab[k].getX() >= 8)
							isEmpty = false;							
					}
					else if(bateauTab[k].getNbCase() == 4)
					{
						if(bateauTab[k].getX() >= 7)
							isEmpty = false;
					}
				}
				
				//si la tete es trop proche du bar on ne fais pas se qui es plus bas on passe a la prochaine iteration
				if(!isEmpty)
					continue;
				
				for(int i = 0 ; i <= bateauTab[k].getNbCase()-1 ; i++)
				{
					if(bateauTab[k].direction == Direction.Vertical)
					{
						if(p2Case[y+i][x] == 1 || p2Case[y+i][x] == 2 || p2Case[y+i][x] == 3 || p2Case[y+i][x] == 4)
							isEmpty = false;	
					}
					else
						if(p2Case[y][x+i] == 1 || p2Case[y][x+i] == 2 || p2Case[y][x+i] == 3 || p2Case[y][x+i] == 4)
							isEmpty = false;			
				}
				
				if(isEmpty)
				{
					isEmpty = true;
					
					for(int i = 0 ; i <= bateauTab[k].getNbCase()-1 ; i++)
					{
						if(bateauTab[k].direction == Direction.Vertical)
							p2Case[y+i][x] = k+1;
						else
							p2Case[y][x+i] = k+1;
					}	
					
					///////////////////DESSIN LES BOUTON SUR LE PANEL//////////////////////
					String caseNber = Integer.toString(bateauTab[k].getX()) + Integer.toString(bateauTab[k].getY());
					
					int caseNberToInt = Integer.parseInt(caseNber);
					
					//la tete du bateau
					//pan22.getComponent(caseNberToInt).setBackground(Color.green);
					bateauTab[k].addPosition(caseNberToInt);
					
					//les case restante du bateau
					if(bateauTab[k].getDirection() == Direction.Vertical)
					{
						for(int i = 0 ; i <= bateauTab[k].getNbCase()-2 ; i++)
						{
							caseNberToInt += 1;
							//pan22.getComponent(caseNberToInt).setBackground(Color.green);
							bateauTab[k].addPosition(caseNberToInt);
						}
					}
					else if(bateauTab[k].getDirection() == Direction.Horizontal)
					{
						for(int i = 0 ; i <= bateauTab[k].getNbCase()-2 ; i++)
						{
							caseNberToInt += 10;
							//pan22.getComponent(caseNberToInt).setBackground(Color.green);
							bateauTab[k].addPosition(caseNberToInt);
						}
					}
					//////////////////////////////////////////////////////////////////////
					
				}
				
			}while(!isEmpty);
		}
	}
	
	public void randomizePositioningPlayer()
	{
		boolean isEmpty = true;
		Direction direction;
		
		
		for(int k = 0 ; k <= 4-1 ; k++)
		{
			do
			{
				isEmpty = true;
				int x = (int) Math.round(Math.random() * 9);
				int y = (int) Math.round(Math.random() * 9);
				int alignement = (int) Math.round(Math.random() * 1);
				
				if(alignement == 0)
					direction = Direction.Vertical;
				else
					direction = Direction.Horizontal;
				
				if(k == 0)		
					bateauTabOwn[0] = new Destroyer(3,direction,x,y);
				else if (k == 1)
					bateauTabOwn[1] = new PorteAvion(4,direction,x,y);
				else if(k == 2)
					bateauTabOwn[2] = new SousMarin(3,direction,x,y);
				else
					bateauTabOwn[3] = new BateauPatrouille(2,direction,x,y);
				
				
				
				if(bateauTabOwn[k].direction == Direction.Vertical)
				{
					if(bateauTabOwn[k].getNbCase() == 2)
					{
						if(bateauTabOwn[k].getY() >= 9)
							isEmpty = false;
					}
					else if(bateauTabOwn[k].getNbCase() == 3)
					{
						if(bateauTabOwn[k].getY() >= 8)
							isEmpty = false;
					}
					else if(bateauTabOwn[k].getNbCase() == 4)
					{
						if(bateauTabOwn[k].getY() >= 7)
							isEmpty = false;
					}
				}
				else
				{
					if(bateauTabOwn[k].getNbCase() == 2)
					{
						if(bateauTabOwn[k].getX() >= 9)
							isEmpty = false;
					}
					else if(bateauTabOwn[k].getNbCase() == 3)
					{
						if(bateauTabOwn[k].getX() >= 8)
							isEmpty = false;							
					}
					else if(bateauTabOwn[k].getNbCase() == 4)
					{
						if(bateauTabOwn[k].getX() >= 7)
							isEmpty = false;
					}
				}
				
				//si la tete es trop proche du bar on ne fais pas se qui es plus bas on passe a la prochaine iteration
				if(!isEmpty)
					continue;
				
				for(int i = 0 ; i <= bateauTabOwn[k].getNbCase()-1 ; i++)
				{
					if(bateauTabOwn[k].direction == Direction.Vertical)
					{
						if(p1Case[y+i][x] == 1 || p1Case[y+i][x] == 2 || p1Case[y+i][x] == 3 || p1Case[y+i][x] == 4)
							isEmpty = false;	
					}
					else
						if(p1Case[y][x+i] == 1 || p1Case[y][x+i] == 2 || p1Case[y][x+i] == 3 || p1Case[y][x+i] == 4)
							isEmpty = false;			
				}
				
				if(isEmpty)
				{
					isEmpty = true;
					
					for(int i = 0 ; i <= bateauTabOwn[k].getNbCase()-1 ; i++)
					{
						if(bateauTabOwn[k].direction == Direction.Vertical)
							p1Case[y+i][x] = k+1;
						else
							p1Case[y][x+i] = k+1;
					}	
					
					
					///////////////////DESSIN LES BOUTON SUR LE PANEL//////////////////////
					String caseNber = Integer.toString(bateauTabOwn[k].getX()) + Integer.toString(bateauTabOwn[k].getY());
					
					int caseNberToInt = Integer.parseInt(caseNber);
					
					//la tete du bateau
					pan21.getComponent(caseNberToInt).setBackground(Color.green);
					bateauTabOwn[k].addPosition(caseNberToInt);
					
					
					//les case restante du bateau
					if(bateauTabOwn[k].getDirection() == Direction.Vertical)
					{
						for(int i = 0 ; i <= bateauTabOwn[k].getNbCase()-2 ; i++)
						{
							caseNberToInt += 1;
							pan21.getComponent(caseNberToInt).setBackground(Color.green);
							bateauTabOwn[k].addPosition(caseNberToInt);
						}
					}
					else if(bateauTabOwn[k].getDirection() == Direction.Horizontal)
					{
						for(int i = 0 ; i <= bateauTabOwn[k].getNbCase()-2 ; i++)
						{
							caseNberToInt += 10;
							pan21.getComponent(caseNberToInt).setBackground(Color.green);
							bateauTabOwn[k].addPosition(caseNberToInt);
						}
					}

					
					//////////////////////////////////////////////////////////////////////
				}
				
				
			}while(!isEmpty);
		}
	}
	
	public void cheat()
	{
		for(int i = 0 ; i <= bateauTab.length-1 ; i++)
		{		
			for(int j = 0 ; j <= bateauTab[i].getList().size()-1 ; j++)
			{
				if(pan22.getComponent(this.bateauTab[i].getList().get(j)).getBackground() == Color.darkGray)
				{
					pan22.getComponent(this.bateauTab[i].getList().get(j)).setBackground(Color.green);
				}				
				else if(pan22.getComponent(this.bateauTab[i].getList().get(j)).getBackground() == Color.green)
					pan22.getComponent(this.bateauTab[i].getList().get(j)).setBackground(Color.darkGray);
				pan22.repaint();
			}
		}
	}
	
	public void setGreenSquare(boolean b)
	{
		this.playerTurn = b;
	}
	
	public Reseau getTurn()
	{
		return this.turnToPlay;
	}
	
	public int getCasePlayed()
	{
		return this.casePlayed;
	}
	
	public boolean havePlayed()
	{
		return this.havePlayed;
	}
	
	//pour setter si le joueur a jouer
	public void setHavePlayed(boolean b)
	{
		this.havePlayed = b;
	}
		
	//pour setter ses a ki de jouer
	public void setTurn(Reseau turn)
	{
		this.turnToPlay = turn;
	}
	
	public void startServer()
	{
		this.server = new Server(this);
	}
	
	public void startClient()
	{
		this.client = new Client(this);
	}

	@Override
	public void keyPressed(KeyEvent e) 
	{
		
		int key = e.getKeyCode();
		
	    if (key == KeyEvent.VK_LEFT)
	    {
	    	startServer();
	    }		
	    
	    if (key == KeyEvent.VK_RIGHT)
	    {
	    	startClient();
	    }
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public Bateau getPorteAvion()
	{
		for(int i = 0 ; i <= this.bateauTabOwn.length-1 ; i++)
		{
			if(bateauTabOwn[i] instanceof PorteAvion)
				return bateauTabOwn[i];
		}
		return null;
	}
	
	public Bateau getDestroyer()
	{
		for(int i = 0 ; i <= this.bateauTabOwn.length-1 ; i++)
		{
			if(bateauTabOwn[i] instanceof Destroyer)
				return bateauTabOwn[i];
		}
		return null;
	}
	
	public Bateau getSousMarin()
	{
		for(int i = 0 ; i <= this.bateauTabOwn.length-1 ; i++)
		{
			if(bateauTabOwn[i] instanceof SousMarin)
				return bateauTabOwn[i];
		}
		return null;
	}
	
	public Bateau getBateauPatrouille()
	{
		for(int i = 0 ; i <= this.bateauTabOwn.length-1 ; i++)
		{
			if(bateauTabOwn[i] instanceof BateauPatrouille)
				return bateauTabOwn[i];
		}
		return null;
	}

	public boolean isEnemyIsReady() {
		return enemyIsReady;
	}

	public void setEnemyIsReady(boolean enemyIsReady) {
		this.enemyIsReady = enemyIsReady;
	}
	
	public void checkReady()
	{
		Thread t = new Thread(new Runnable()
		{
			@Override
			public void run() 
			{
				boolean notStart = true;
				while(notStart)
				{
					if(enemyIsReady && sp.isPlayer1Ready())
					{
						throwStartTimer();
						notStart = false;
						bp.getReady().setEnabled(false);
						bp.getRandom().setEnabled(false);
						bp.getReset().setEnabled(false);
					}
						
				}
			}	
		});
		
		t.start();
	}
	
	public void dialog(boolean win)
	{
		if(win)
			JOptionPane.showMessageDialog(null, "You have win" , "Game end" , JOptionPane.INFORMATION_MESSAGE);
		else
			JOptionPane.showMessageDialog(null, "You have loose" , "Game end" , JOptionPane.INFORMATION_MESSAGE);
		
		((JFrame)getRootPane().getParent()).dispose();
	}
}
