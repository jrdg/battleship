import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class BoatPickerPanel extends JPanel
{
	JPanel pickpan;
	JPanel pickpanCenter;
	JPanel pickpanWest;
	JButton random;
	JButton ready;
	JButton reset;
	JTabbedPane jtab;
	private LogPanel lp;
	
	public BoatPickerPanel()
	{
		jtab = new JTabbedPane();
		this.setBorder(BorderFactory.createEmptyBorder(25, 25, 25, 25));
		this.setPreferredSize(new Dimension(1200,300));
		this.setLayout(new GridLayout(1,2));
		pickpan = new JPanel();
		pickpan.setOpaque(false);
		pickpan.setLayout(new BorderLayout());
		pickpanWest = new JPanel();
		pickpanWest.setLayout(new GridLayout(3,1,5,5));
		pickpanWest.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		pickpanWest.setBackground(Color.darkGray);
		random = new JButton("Randomize");
		pickpanWest.add(random);
		ready = new JButton("Ready");
		pickpanWest.add(ready);
		reset = new JButton("Reset boat");
		pickpanWest.add(reset);
		pickpan.add(pickpanWest,BorderLayout.WEST);
		pickpanCenter = new JPanel();
		pickpanCenter.setLayout(new BorderLayout());
		pickpanCenter.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		JPanel try1 = new JPanel();
		lp = new LogPanel();
		try1.setBackground(Color.red);
		jtab.add("Boat pick", try1);
		jtab.add("Log", lp);
		jtab.setFocusable(false);
		pickpanCenter.add(jtab);
		pickpanCenter.setBackground(Color.darkGray);
		pickpan.add(pickpanCenter,BorderLayout.CENTER);
		this.add(pickpan);
	}
	
	public void paintComponent(Graphics g)
	{
		g.setColor(Color.darkGray);
		
		g.fillRect(20, 25, this.getWidth()-40, 250);
	}
	
	public JButton getRandom()
	{
		return this.random;
	}
	
	public JButton getReady()
	{
		return this.ready;
	}
	
	public JButton getReset()
	{
		return this.reset;
	}
	
	public LogPanel getLogPanel()
	{
		return this.lp;
	}
}
