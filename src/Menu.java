import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

public class Menu extends JFrame
{
	private JButton playSolo;
	private JButton multiplayer;
	private JButton rules;
	private JButton hof;
	private JButton quit;
	private JButton setting;
	private JPanel buttonPanel;
	private int difficulty;
	private int mode;
	private MouseListener ms;
	private ActionListener action;
	
	public Menu(int difficulty , int mode)
	{
		this.difficulty = difficulty;
		this.mode = mode;
		System.out.println(this.difficulty);
		this.setTitle("Battleship");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1920,1080);
		this.setLocationRelativeTo(null);
		JPanel main = new JPanel();
		this.setContentPane(main);
		MenuLogo ml = new MenuLogo();
		ml.setPreferredSize(new Dimension(this.getWidth(),270));
		this.getContentPane().add(ml);
		playSolo = new JButton("Solo");
		playSolo.setFocusable(false);
		playSolo.setPreferredSize(new Dimension(this.getWidth(),80));
		playSolo.setBackground(Color.black);
		playSolo.setForeground(Color.white);
		playSolo.setBorderPainted(false);
		playSolo.setFont(new Font("Arial", Font.PLAIN, 40));
		multiplayer = new JButton("Multiplayers");
		multiplayer.setFocusable(false);
		multiplayer.setPreferredSize(new Dimension(this.getWidth(),80));
		multiplayer.setBackground(Color.black);
		multiplayer.setForeground(Color.white);
		multiplayer.setBorderPainted(false);
		multiplayer.setFont(new Font("Arial", Font.PLAIN, 40));
		hof = new JButton("Hall of Fame");
		hof.setFocusable(false);
		hof.setPreferredSize(new Dimension(this.getWidth(),80));
		hof.setBackground(Color.black);
		hof.setForeground(Color.white);
		hof.setBorderPainted(false);
		hof.setFont(new Font("Arial", Font.PLAIN, 40));
		rules = new JButton("Rules");
		rules.setFocusable(false);
		rules.setPreferredSize(new Dimension(this.getWidth(),80));
		rules.setBackground(Color.black);
		rules.setForeground(Color.white);
		rules.setBorderPainted(false);
		rules.setFont(new Font("Arial", Font.PLAIN, 40));		
		setting = new JButton("Setting");
		setting.setFocusable(false);
		setting.setPreferredSize(new Dimension(this.getWidth(),80));
		setting.setBackground(Color.black);
		setting.setForeground(Color.white);
		setting.setBorderPainted(false);
		setting.setFont(new Font("Arial", Font.PLAIN, 40));		
		quit = new JButton("Quit game");
		quit.setFocusable(false);
		quit.setPreferredSize(new Dimension(this.getWidth(),80));
		quit.setBackground(Color.black);
		quit.setForeground(Color.white);
		quit.setBorderPainted(false);
		quit.setFont(new Font("Arial", Font.PLAIN, 40));
		
		action = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{				
		        if(((JButton)e.getSource()).getText() == "Solo")
		        {
		        	GameFrame gp = new GameFrame(difficulty,mode);
		        	dispose();
		        }
		        else if(((JButton)e.getSource()).getText() == "Multiplayers")
		        	new MultiFrame(Reseau.Server);
		        else if(((JButton)e.getSource()).getText() == "Hall of Fame")
		        	new HofFrame();
		        else if(((JButton)e.getSource()).getText() == "Setting")
		        {
					setEnabled(false);
					SettingWindow sw = new SettingWindow(difficulty,mode);
					dispose();
		        }
		        else if(((JButton)e.getSource()).getText() == "Rules")
		        	System.out.println("rules click");
		        else if(((JButton)e.getSource()).getText() == "Quit game")
		        {
					int n = JOptionPane.showConfirmDialog(
							   null,
							    "Are you sure you want to quit game ?",
							    "Quit game confirmation",
							    JOptionPane.YES_NO_OPTION);
						
						if(n == 0)
							System.exit(0);
		        }
	        }
		};
		
		ms = new MouseListener()
		{

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				((JButton)e.getSource()).setBackground(Color.gray);					
			}

			@Override
			public void mouseExited(MouseEvent e) {
				((JButton)e.getSource()).setBackground(Color.black);	
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
						
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
						
			}
			
		};
		
		playSolo.addMouseListener(ms);
		rules.addMouseListener(ms);
		hof.addMouseListener(ms);
		multiplayer.addMouseListener(ms);
		quit.addMouseListener(ms);
		setting.addMouseListener(ms);	
		playSolo.addActionListener(action);
		multiplayer.addActionListener(action);
		hof.addActionListener(action);
		rules.addActionListener(action);
		quit.addActionListener(action);
		setting.addActionListener(action);
		
		
		main.add(playSolo);
		main.add(multiplayer);
		main.add(hof);
		main.add(rules);
		main.add(setting);
		main.add(quit);
		main.setBackground(Color.black);
		
		this.setVisible(true);

		
		this.addWindowListener(new java.awt.event.WindowAdapter()
		{
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent)
		    {
				int n = JOptionPane.showConfirmDialog(
						   null,
						    "Are you sure you want to quit game ?",
						    "Quit game confirmation",
						    JOptionPane.YES_NO_OPTION);
					
					if(n == 0)
						System.exit(0);
					else
						setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE );
		    }
		});
	}
}
