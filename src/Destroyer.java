
public class Destroyer extends Bateau
{
	public Destroyer(int nbCase, Direction direction,int x , int y) 
	{
		super(nbCase, direction,x,y);
	}
	
	public int getCase1()
	{
		return this.getList().get(0);
	}
	
	public int getCase2()
	{
		return this.getList().get(1);
	}
	
	public int getCase3()
	{
		return this.getList().get(2);
	}

}
