import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class SettingWindow extends JFrame
{
	private JComboBox<String> cbdiff;
	private JComboBox<String> cbMode;
	private JButton reset;
	private JButton save;
	private int difficulty;
	private int mode;
	private Menu men;
	private JLabel diff;
	private JLabel mod;
	
	public SettingWindow(int difficulty,int mode)
	{
		this.difficulty = difficulty;
		this.mode = mode;
		this.setResizable(false);
		this.setTitle("Battleship setting");
		this.setSize(500, 150);
		this.setLocationRelativeTo(null);
		
		//le panel qui englobe tous les autre
		JPanel globalPanel = new JPanel();
		globalPanel.setLayout(new GridLayout(3,0));
		
		JPanel p2 = new JPanel();
		p2.setBackground(Color.black);
		diff = new JLabel("Difficulty");
		diff.setForeground(Color.white);
		p2.add(diff);
		cbdiff = new JComboBox();
		cbdiff.addItem("Easy");
		cbdiff.addItem("Medium");
		cbdiff.addItem("Hard");
		cbdiff.setSelectedIndex(this.difficulty);
		p2.add(cbdiff);
		globalPanel.add(p2);
		
		JPanel p3 = new JPanel();
		mod = new JLabel("Game mode: ");
		mod.setForeground(Color.white);
		p3.add(mod);
		p3.setBackground(Color.black);
		cbMode = new JComboBox();
		cbMode.addItem("Normal");
		cbMode.addItem("Hardcore");
		cbMode.setSelectedIndex(this.mode);
		p3.add(cbMode);
		globalPanel.add(p3);
		
		JPanel p4 = new JPanel();
		p4.setBackground(Color.black);
		save = new JButton("save");
		save.setBackground(Color.black);
		save.setForeground(Color.white);
		save.setFocusable(false);
		p4.add(save);
		reset = new JButton("Default");
		reset.setBackground(Color.black);
		reset.setForeground(Color.white);
		reset.setFocusable(false);
		p4.add(reset);	
		globalPanel.add(p4);
			
		this.setContentPane(globalPanel);
			
		this.setVisible(true);
	
		this.resetAction();
		this.saveAction();
		
		this.addWindowListener(new java.awt.event.WindowAdapter()
		{
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent)
		    {
		    	dispose();
		    	
				int n = JOptionPane.showConfirmDialog(
						   null,
						    "Do you want ot save setting ?",
						    "save setting confirmation",
						    JOptionPane.YES_NO_OPTION);
					
					if(n == 0)
						men = new Menu(cbdiff.getSelectedIndex(),cbMode.getSelectedIndex());
					else
						men = new Menu(difficulty,mode);
		    }
		});
	}
	
	private void resetAction()
	{
		reset.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				cbdiff.setSelectedIndex(0);
				cbMode.setSelectedIndex(0);					
			}
			
		});
	}
	
	private void saveAction()
	{
		save.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{			
				dispose();
				
				int n = JOptionPane.showConfirmDialog(
						   null,
						    "Do you want to save setting ?",
						    "Save setting confirmation",
						    JOptionPane.YES_NO_OPTION);
					
					if(n == 0)
						men = new Menu(cbdiff.getSelectedIndex(),cbMode.getSelectedIndex());
					else
						men = new Menu(difficulty,mode);
			}
			
		});
	}
}
