import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

public class GameFrame extends JFrame
{
	private int difficulty;
	private int mode;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem reset;
	private JMenuItem exit;
	private JMenuItem pause;
	private GamePanel gp;
	
	public GameFrame(int difficulty , int mode)
	{
		this.difficulty = difficulty;
		menuBar = new JMenuBar();
		menu = new JMenu("Menu");
		reset = new JMenuItem("Reset");
		exit = new JMenuItem("Exit");
		pause = new JMenuItem("Pause");
		gp = new GamePanel(this.difficulty,this.mode);
		menu.add(pause);
		menu.add(reset);
		menu.add(exit);
		menuBar.add(menu);
		this.setJMenuBar(menuBar);
		this.setResizable(false);
		this.mode = mode;
		this.setTitle("Battleship");
		this.setSize(1200, 1000);
		this.setLocationRelativeTo(null);
		
		this.setContentPane(gp);
		this.setVisible(true);
		
		this.exit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
					int n = JOptionPane.showConfirmDialog(
						   null,
						    "Are you sure you want to return to menu",
						    "Quit game confirmation",
						    JOptionPane.YES_NO_OPTION);
					
					if(n == 0)
					{
						gp.getSp().getMinObject().stop();
						new Menu(difficulty,mode);
						dispose();
					}
			}
			
		});
		
		this.reset.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(gp.getIsStart())
				{
					int n = JOptionPane.showConfirmDialog(null, "Are you sure you want to restart the game ?","restart message",JOptionPane.YES_NO_OPTION);
					
					if(n == JOptionPane.YES_OPTION)
						gp.resetGame();
				}
				else
					JOptionPane.showMessageDialog(null, "The game is not started","error",JOptionPane.ERROR_MESSAGE);
				
			}
			
		});
		
		
		//si on ferme la parti en cliquant sur le x
		this.addWindowListener(new java.awt.event.WindowAdapter()
		{
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent)
		    {
				int n = JOptionPane.showConfirmDialog(
						   null,
						    "Are you sure you want to return to menu",
						    "Quit game confirmation",
						    JOptionPane.YES_NO_OPTION);
					
					if(n == 0)
					{
						gp.getSp().getMinObject().stop();
						new Menu(difficulty,mode);
						dispose();
					}
						
					else
						setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE );
		    }
		});
	}
	
	public void paintComponent(Graphics g)
	{
		g.setColor(Color.red);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
	}
}
