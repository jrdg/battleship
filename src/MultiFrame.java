import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

public class MultiFrame extends JFrame
{
	private int difficulty;
	private int mode;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem reset;
	private JMenuItem exit;
	private JMenuItem pause;
	private MultiPanel mp;
	private Reseau reseau;
	
	public MultiFrame(Reseau reseau)
	{
		this.reseau = reseau;
		this.difficulty = difficulty;
		menuBar = new JMenuBar();
		menu = new JMenu("Menu");
		reset = new JMenuItem("Reset");
		exit = new JMenuItem("Exit");
		pause = new JMenuItem("Pause");
		mp = new MultiPanel(this.reseau);
		menu.add(pause);
		menu.add(reset);
		menu.add(exit);
		menuBar.add(menu);
		this.setJMenuBar(menuBar);
		this.setResizable(false);
		this.mode = mode;
		this.setTitle("Battleship");
		this.setSize(1200, 1000);
		this.setLocationRelativeTo(null);
		
		this.setContentPane(mp);
		this.setVisible(true);
		
		this.exit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
					int n = JOptionPane.showConfirmDialog(
						   null,
						    "Are you sure you want to return to menu",
						    "Quit game confirmation",
						    JOptionPane.YES_NO_OPTION);
					
					if(n == JOptionPane.YES_OPTION)
					{
						new Menu(0,0);
						dispose();
					}
			}
			
		});
	}
	
	public void paintComponent(Graphics g)
	{
		g.setColor(Color.red);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
	}
}