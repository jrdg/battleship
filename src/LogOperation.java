import java.util.Date;

public class LogOperation 
{
	private Date date;
	private boolean aTouche;
	private boolean aTuer;
	private static int quelleTour = 1;
	private int sonTour;
	private int caseTouch;
	private Bateau boat;
	private boolean isPlayer;
	
	public LogOperation(boolean aTouche , boolean aTuer,boolean isPlayer,int caseTouch , Bateau boat)
	{
		date = new Date();
		this.isPlayer = isPlayer;
		this.aTouche = aTouche;
		this.aTuer = aTuer;
		this.caseTouch = caseTouch;
		this.boat = boat;
		sonTour = this.quelleTour;
		quelleTour++;
	}
	
	@Override
	public String toString()
	{
		String tour = "st";
		String msgTouche = "";
		String b = "";
		String playerOrBot = "";
		
		if(isPlayer)
		{
			msgTouche = "You can do better! lets try again the next turn.";
			playerOrBot = "You";
		}
		else
		{
			msgTouche = "Enemy have fail us";
			playerOrBot = "Enemy";
		}
					
		if(boat instanceof Destroyer)
			b = "Destroyer";
		else if(boat instanceof PorteAvion)
			b = "PorteAvion";
		else if(boat instanceof SousMarin)
			b = "Sous marin";
		else if(boat instanceof BateauPatrouille)
			b = "Bateau de patrouille";
		
		if(aTuer)
			msgTouche = playerOrBot+" have destroy the "+b;
		if(aTouche)
			msgTouche = playerOrBot+"  have touch the "+b+" at the case : "+caseTouch;
		
		return date.toString()+" : "+sonTour+" "+tour+" operation : "+msgTouche;
	}
	
}
