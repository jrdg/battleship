import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatsPanel extends JPanel
{
	private int time;
	private Minuterie min;
	
	//p1
	private int scoreP1;
	private int ownShipDieP1;
	private int shipKilledP1;
	
	//p2
	private int scoreP2;
	private int ownShipDieP2;
	private int shipKilledP2;
	
	private boolean playerTurn;
	
	//multiplayer
	boolean isMulti;
	boolean player1Ready;
	boolean player2Ready;
	
	public StatsPanel()
	{
		this.isMulti = false;
		this.player1Ready = false;
		this.player2Ready = false;
		min = new Minuterie(0.00);
		
		//p1
		this.scoreP1 = 0;
		this.ownShipDieP1 = 0;
		this.shipKilledP1 = 0;
		
		//p2
		this.scoreP2 = 0;
		this.ownShipDieP2 = 0;
		this.shipKilledP2 = 0;
		
		this.setPreferredSize(new Dimension(1200,200));
		
		this.time = -1;
	}
	
	public boolean isPlayer1Ready() {
		return player1Ready;
	}

	public void setPlayer1Ready(boolean player1Ready) {
		this.player1Ready = player1Ready;
	}

	public boolean isPlayer2Ready() {
		return player2Ready;
	}

	public void setPlayer2Ready(boolean player2Ready) {
		this.player2Ready = player2Ready;
	}

	public void paintComponent(Graphics g)
	{	
		g.setColor(Color.black);
		g.fillRect(0, 0, 1200, 1000);
		
		g.setFont(new Font("Arial", Font.PLAIN, 18));
		g.setColor(Color.white);	
		
		//p1	
		g.drawRect(100, 20, 400, 150);
		g.drawString("Player 1", 270, 40);
		g.drawString("Score: "+this.scoreP1, 110, 60);
		g.drawString("Ship killed: "+this.shipKilledP1, 110, 80);
		g.drawString("own ship killed: "+this.ownShipDieP1, 110, 100);
		g.setFont(new Font("Arial", Font.PLAIN, 15));
		
		if(this.isMulti)
		{
			if(!this.player1Ready)
				g.setColor(Color.red);
			else
				g.setColor(Color.green);
			
			g.drawString("READY", 400, 150);
			
			
			if(!this.player2Ready)
				g.setColor(Color.red);
			else
				g.setColor(Color.green);
			
			g.drawString("READY", 990, 150);
		}
		
		g.setColor(Color.white);

		
		g.drawString("    0            1            2             3            4            5            6             7           8            9" , 25,195);
		g.drawString("    0           1            2             3            4            5            6             7           8            9" , 625,195);
		
		g.setFont(new Font("Arial", Font.PLAIN, 18));
		if(playerTurn ) //&& this.min.isStarted()
			g.setColor(Color.green);
		else
			g.setColor(Color.gray);
		
		g.fillRect(110, 120, 30, 30);
		
		g.setColor(Color.white);
		
		//time
		if(time == -1)
			g.drawString("Press ready for start", 510, 120);
		else if(time > 0)
			g.drawString(Integer.toString(time), 585, 120);
		else
		{
			g.drawString("Game started", 540, 120);
			g.drawString(min.getTimeString(), 580, 140);
		}
			
		
		//p2
		g.drawRect(680, 20, 400, 150);
		g.drawString("Player 2", 850, 40);
		g.drawString("Score: "+this.scoreP2, 690, 60);
		g.drawString("Ship killed: "+this.shipKilledP2, 690, 80);
		g.drawString("own ship killed: "+this.ownShipDieP2, 690, 100);
		
		if(!playerTurn)
			g.setColor(Color.green);
		else
			g.setColor(Color.gray);
		
		g.fillRect(690, 120, 30, 30);
	}

	public int getScoreP1() {
		return scoreP1;
	}

	public void setScoreP1(int scoreP1) {
		this.scoreP1 = scoreP1;
	}

	public int getOwnShipDieP1() {
		return ownShipDieP1;
	}

	public void setOwnShipDieP1(int ownShipDieP1) {
		this.ownShipDieP1 = ownShipDieP1;
	}

	public int getShipKilledP1() {
		return shipKilledP1;
	}

	public void setShipKilledP1(int shipKilledP1) {
		this.shipKilledP1 = shipKilledP1;
	}

	public int getScoreP2() {
		return scoreP2;
	}

	public void setScoreP2(int scoreP2) {
		this.scoreP2 = scoreP2;
	}

	public int getOwnShipDieP2() {
		return ownShipDieP2;
	}

	public void setOwnShipDieP2(int ownShipDieP2) {
		this.ownShipDieP2 = ownShipDieP2;
	}

	public int getShipKilledP2() {
		return shipKilledP2;
	}

	public void setShipKilledP2(int shipKilledP2) {
		this.shipKilledP2 = shipKilledP2;
	}
	
	public boolean getPlayerTurn()
	{
		return this.playerTurn;
	}
	
	public void setPlayerTurn(boolean pt){
		this.playerTurn = pt;
	}
	
	public int getTime()
	{
		return this.time;
	}
	
	public void setTime(int time)
	{
		this.time = time;
	}
	
	public Minuterie getMinObject()
	{
		return this.min;
	}
	
	public boolean getIsMulti()
	{
		return this.isMulti;
	}
	
	public void setMulti(boolean b)
	{
		this.isMulti = b;
	}
}
