
public class Player 
{
	private String name;
	private int score;
	private int shipDestroyed;
	private int ownShipDestroyed;
	
	public Player(String name)
	{
		this.name = name;
		this.score = 0;
		this.shipDestroyed = 0;
		this.ownShipDestroyed = 0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getShipDestroyed() {
		return shipDestroyed;
	}

	public void setShipDestroyed(int shipDestroyed) {
		this.shipDestroyed = shipDestroyed;
	}

	public int getOwnShipDestroyed() {
		return ownShipDestroyed;
	}

	public void setOwnShipDestroyed(int ownShipDestroyed) {
		this.ownShipDestroyed = ownShipDestroyed;
	}
}
