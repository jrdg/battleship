import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client 
{
	Socket socket;
	PrintWriter out;
	BufferedReader in;
	MultiPanel mp;
	boolean brokeFirst;
	boolean brokeFirstRead;
	
	int porteAvion1;
	int porteAvion2;
	int porteAvion3;
	int porteAvion4;
	
	int destroyer1;
	int destroyer2;
	int destroyer3;
	
	int bp1;
	int bp2;
	
	int sousmarin1;
	int sousmarin2;
	int sousmarin3;
	
	int[] porteAvionPos = {porteAvion1,porteAvion2,porteAvion3,porteAvion4};
	int[] destroyerPos = {destroyer1,destroyer2,destroyer3};
	int[] sousMarinPos = {sousmarin1,sousmarin2,sousmarin3};
	int[] bateauPatrouillePos = {bp1,bp2};
	
	Bateau[] bateautab = new Bateau[4];
	
	Bateau porteavion;
	Bateau dest;
	Bateau sousmarin;
	Bateau bp;
	
	public Client(MultiPanel mp)
	{
		this.mp = mp;
		porteavion = new PorteAvion(4,Direction.Horizontal,0,0);
		dest = new Destroyer(3,Direction.Horizontal,0,0);
		sousmarin = new SousMarin(3,Direction.Horizontal,0,0);
		bp = new BateauPatrouille(2,Direction.Horizontal,0,0);
		bateautab[0] = porteavion;
		bateautab[1] = dest;
		bateautab[2] = sousmarin;
		bateautab[3] = bp;
		this.startClient();
	}
	
		public void startClient()
		{
	
			try 
			{
				System.out.println("Client : avant connection");
	
				socket = new Socket("127.0.0.1", 30000);
				System.out.println("Client : apres connection");
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(),true);
				String msgSend = "";
				String msg = "";
				
				Thread tRead = new Thread(new Runnable()
				{
					@Override
					public void run() 
					{
						String msg = "";
						String turn = "";
						
						//tant que lautre joueur nes pas pret
						while(!brokeFirstRead)
						{
							try 
							{
								msg = in.readLine();
							} 
							catch (IOException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							if(msg.equals("ready"))
							{
								System.out.println("server ready");
								mp.getSp().setPlayer2Ready(true);
								mp.getSp().repaint();
								mp.setEnemyIsReady(true);
								brokeFirstRead = true;
							}	
						}
						
						System.out.println("fin");
						
						try {
							porteAvion1 = Integer.parseInt(in.readLine());
							porteAvion2 = Integer.parseInt(in.readLine());
							porteAvion3 = Integer.parseInt(in.readLine());
							porteAvion4 = Integer.parseInt(in.readLine());
							
							destroyer1 = Integer.parseInt(in.readLine());
					     	destroyer2 = Integer.parseInt(in.readLine());
							destroyer3 = Integer.parseInt(in.readLine());
							
							sousmarin1 = Integer.parseInt(in.readLine());
					     	sousmarin2 = Integer.parseInt(in.readLine());
							sousmarin3 = Integer.parseInt(in.readLine());
							
							bp1 = Integer.parseInt(in.readLine());
					     	bp2 = Integer.parseInt(in.readLine());
					     	
						} catch (NumberFormatException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						porteavion.addPosition(porteAvion1);
						porteavion.addPosition(porteAvion2);
						porteavion.addPosition(porteAvion3);
						porteavion.addPosition(porteAvion4);
						
						dest.addPosition(destroyer1);
						dest.addPosition(destroyer2);
						dest.addPosition(destroyer3);
						
						sousmarin.addPosition(sousmarin1);
						sousmarin.addPosition(sousmarin2);
						sousmarin.addPosition(sousmarin3);
						
						bp.addPosition(bp1);
						bp.addPosition(bp2);
						
						System.out.println("porteAvion "+porteAvion1);
						System.out.println("pa "+porteAvion2);
						System.out.println("pa "+porteAvion3);
						System.out.println("pa "+porteAvion4);
						
						System.out.println("destroyer "+destroyer1);
						System.out.println("destroyer "+destroyer2);
						System.out.println("destroyer "+destroyer3);
						
						System.out.println("sm "+sousmarin1);
						System.out.println("sm "+sousmarin2);
						System.out.println("sm "+sousmarin3);
						
						System.out.println("bp "+bp1);
						System.out.println("bp "+bp2);
						
						while (msgSend.compareTo("stop") != 0) 
						{
							String touch = "";
							
							try 
							{
								msg = in.readLine();
								touch = in.readLine();
							} 
							catch (IOException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							System.out.println("JUSTE VOIR : "+mp.getTurn());
							if(isInt(msg) && mp.getTurn() == Reseau.Server)
							{
								System.out.println("WE ARE IN");
								mp.getSp().setPlayerTurn(true);
								mp.getSp().repaint();
								mp.setTurn(Reseau.Client);
								System.out.println(msg);
								
								if(touch.equals("nottouch"))
									mp.pan21.getComponent(Integer.parseInt(msg)).setBackground(Color.lightGray);
								else if(touch.equals("touch"))
									mp.pan21.getComponent(Integer.parseInt(msg)).setBackground(Color.orange);
								else if(touch.equals("kill"))
								{
									for(int i = 0 ; i <= mp.bateauTabOwn.length-1 ; i++)
									{
										for(int j = 0 ; j <= mp.bateauTabOwn[i].getList().size()-1 ; j++)
										{
											if(Integer.parseInt(msg) == mp.bateauTabOwn[i].getList().get(j))
											{
												for(int k = 0 ; k <= mp.bateauTabOwn[i].getList().size()-1 ; k++)
												{
													mp.pan21.getComponent(mp.bateauTabOwn[i].getList().get(k)).setBackground(Color.red);
												}
											}
										}
									}
								}
								else if(touch.equals("win"))
								{
									mp.dialog(false);
								}
								
								msg = "";
							}
								
						}
					}			
				});
				
				tRead.start();
				
				Thread tWrite = new Thread(new Runnable()
				{
					@Override
					public void run() 
					{
						
						while(!brokeFirst)
						{
							if(mp.getSp().player1Ready)
							{
								System.out.println("envoie vers le serveur");
								out.println("ready");
								brokeFirst = true;
							}	
						}
						
						System.out.println("fin premiere boucle");
						
						//on donne a serveur les position d
						out.println(((PorteAvion)mp.getPorteAvion()).getCase1());
						out.println(((PorteAvion)mp.getPorteAvion()).getCase2());
						out.println(((PorteAvion)mp.getPorteAvion()).getCase3());
						out.println(((PorteAvion)mp.getPorteAvion()).getCase4());
						
						out.println(((Destroyer)mp.getDestroyer()).getCase1());
						out.println(((Destroyer)mp.getDestroyer()).getCase2());
						out.println(((Destroyer)mp.getDestroyer()).getCase3());
						
						out.println(((SousMarin)mp.getSousMarin()).getCase1());
						out.println(((SousMarin)mp.getSousMarin()).getCase2());
						out.println(((SousMarin)mp.getSousMarin()).getCase3());
						
						out.println(((BateauPatrouille)mp.getBateauPatrouille()).getCase1());
						out.println(((BateauPatrouille)mp.getBateauPatrouille()).getCase2());
						
						while (mp.pts != 4) 
						{	
							if(mp.getTurn() == Reseau.Client)
							{
								if(mp.havePlayed())
								{
									System.out.println("inner");
									mp.getSp().setPlayerTurn(false);
									mp.getSp().repaint();
									mp.setTurn(Reseau.Server);
									out.println(mp.getCasePlayed());
									
									mp.pan22.getComponent(mp.getCasePlayed()).setBackground(Color.lightGray);
									
									boolean istouch = false;
									
									for(int i = 0 ; i <= bateautab.length-1 ; i++)
									{
										if(bateautab[i].isTouch(mp.getCasePlayed()))
										{
											System.out.println("touch");
											
											System.out.println(bateautab[i].life);
											if(!bateautab[i].isAlive)
											{
												mp.pts++;
												for(int l = 0 ; l <= bateautab[i].getList().size()-1 ; l++)
												{
													mp.pan22.getComponent(bateautab[i].getList().get(l)).setBackground(Color.red);;
												}
												if(mp.pts == 4)
													out.println("win");
												else
													out.println("kill");
											}
											else
											{
												mp.pan22.getComponent(mp.getCasePlayed()).setBackground(Color.ORANGE);
												out.println("touch");
											}	
										}
									}
									
									if(!istouch)
										out.println("nottouch");
									
									mp.setHavePlayed(false);
								}

							}
						}	
						
						mp.dialog(true);
					}			
				});
				
				tWrite.start();

			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public void flushIt()
		{
			this.out.close();
			try {
				this.in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public boolean isInt(String s)
		{
			boolean isint = true;
			
			try
			{
				int n = Integer.parseInt(s);
			}
			catch(NumberFormatException e)
			{
				isint = false;
			}
			
			return isint;
		}
}