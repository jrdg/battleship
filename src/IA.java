import java.util.ArrayList;

public class IA 
{
	private ArrayList<Bateau> tableau; //array list qui contient les bateau (ses ici que le add se fais)
	private ArrayList<Integer> startPos; //arraylist qui va prendre la premiere case du bateau clicker par le user
	private int[] randcaseTab; //tableaui va contenir les case random a choisir au jusqua temps quil trouve une direction
	private boolean dirContinue;
	private int lastCase;
	private int nextCase;
	private int tempStart;
	private int incY;
	private int incX;
	private Direction d;
	private boolean turnBack;
	private String side;
	
	public IA()
	{
		this.side = "";
		this.turnBack = false;
		incY = 10;
		incX = 1;
		this.tempStart = -1;
		this.nextCase = -1;
		this.lastCase = -1;
		this.dirContinue = false;
		this.tableau = new ArrayList();
		this.startPos = new ArrayList();
		randcaseTab = new int[4];
	}
	
	//bTab = liste des objet bateau qui contienne les position des bateau du player 1
	public int mediumLvl()
	{	
		return 1; //retourne la case ou jouer
	}
	
	public int HardLvl()
	{
		int toReturn = -1;
		
		for(int i = 0 ; i <= tableau.size()-1 ; i++)
		{		
			System.out.println("on entre dans la boucle");
			int[] NsEwTab = {this.startPos.get(i)-10,this.startPos.get(i)-1,this.startPos.get(i)+10,this.startPos.get(i)+1};
			
			//si ce bateau es en vie  on continue avec celui la
			if(tableau.get(i).getIsAlive())
			{
				//on lui affecte une valeur random entre le nort south east west
				
				if(!this.dirContinue)
				{
					toReturn = NsEwTab[((int) Math.round(Math.random() * 3))]; //tr 1
		
					
					if(turnBack)
					{
						if(this.side == "left")
						{
							System.out.println("supposer etre a : "+(this.startPos.get(i)+1));
							toReturn = this.startPos.get(i)+1;
						}
						else if(this.side == "right")
						{
							System.out.println("supposer etre a : "+(this.startPos.get(i)-1));
							toReturn = this.startPos.get(i)-1;
						}
						else if(this.side == "up")
						{
							System.out.println("supposer etre a : "+(this.startPos.get(i)+10));
							toReturn = this.startPos.get(i)+10;
						}
						else if(this.side == "down")
						{
							System.out.println("supposer etre a : "+(this.startPos.get(i)-10));
							toReturn = this.startPos.get(i)-10;
						}
						
						this.turnBack = false;
						incX = 1;
						incY = 10;
					}
				}			
				else
				{
					toReturn = this.nextCase;
				}
					
				
				/*
				 * si cette valeur est encore une fois une case du bateau
				 * on continue dans cette direction
				 */
				if(tableau.get(i).getList().contains(toReturn))
				{
					this.dirContinue = true;
					this.lastCase = toReturn;
					
					if(toReturn == this.startPos.get(i)-this.incY)
					{
						this.nextCase = toReturn-10;
						this.d = Direction.Vertical;
						this.side = "up";
					}
					else if(toReturn == this.startPos.get(i)-this.incX)
					{
						this.nextCase = toReturn-1;
						this.d = Direction.Horizontal;
						this.side = "left";
					}
						
					else if(toReturn == this.startPos.get(i)+this.incY)
					{
						this.nextCase = toReturn+10;
						this.d = Direction.Vertical;
						this.side = "down";
					}	
					else if(toReturn == this.startPos.get(i)+this.incX)
					{
						this.nextCase = toReturn+1;
						this.d = Direction.Horizontal;
						this.side = "right";
					}
						
					
					incX += 1;
					incY += 10;
				}
				else
				{
					if(this.dirContinue)
					{
						System.out.println("side : "+this.side);
						System.out.println("Direction : " + this.d);
						this.turnBack = true;
					}
						
					
					this.dirContinue = false;
				}
				
				if(this.tableau.get(i).getLife() == 1)
				{
					if(this.tableau.get(i).checkTouch(toReturn))
					{
						this.tableau.remove(i);
						this.startPos.remove(i);
						this.dirContinue = false;
						incX = 1;
						incY = 10;
					}
				}
															
				break;
			}
			else
			{

			}
		}	
		
		if(toReturn == -1)
		{
			for(int i = 0 ; i <= tableau.size()-1 ; i++)
				System.out.println(tableau.get(i).getIsAlive());
		}
		
		System.out.println("ON RETOURNE LA CASE : "+toReturn);
		return toReturn; //retourne la case a jouer
	}
	
	public ArrayList<Bateau> getBateauList()
	{
		return this.tableau;
	}
	
	public ArrayList<Integer>  getStartPosList()
	{
		return this.startPos;
	}
	
	public void addBateau(Bateau b)
	{
		this.tableau.add(b);
	}
	
	public void removeBateau(int i)
	{
		this.tableau.remove(i);
	}
	
	public void addStartPos(int pos)
	{
		this.startPos.add(pos);
	}
	
	public void removeStartPos(int i)
	{
		this.startPos.remove(i);
	}
	
	public boolean isContainingBoat()
	{
		boolean isAllNull = true;
		
		for(int i = 0 ; i <= tableau.size()-1 ; i++)
		{
			if(tableau.get(i).getIsAlive())
				return true;
		}
		
		return false;
	}
	
	public void IAreset()
	{
		incY = 10;
		incX = 1;
		this.tempStart = -1;
		this.nextCase = -1;
		this.lastCase = -1;
		this.dirContinue = false;
		this.tableau.clear();
		this.startPos.clear();
		randcaseTab = new int[4];
	}
}
