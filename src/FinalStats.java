
public class FinalStats 
{
	private FileManager fm;
	private String wld;
	private boolean isSolo;
	private int scoreP1;
	private int scoreP2;
	private int difficulty;
	
	public FinalStats(String wld , boolean isSolo, int scoreP1 , int scoreP2, int difficulty)
	{
		this.wld = wld;
		this.isSolo = isSolo;
		this.scoreP1 = scoreP1;
		this.scoreP2 = scoreP2;
		this.difficulty = difficulty;
		this.fm = new FileManager("stats","important");
	}
	
	public FinalStats()
	{
		this.fm = new FileManager("stats","important");
	}
	
	public void addStats()
	{
		String wol = "";
		String som = "";
		String diff = "";
		
		if(this.wld == "win")
			wol = "win";
		else if(this.wld == "draw")
			wol = "draw";
		else
			wol = "loss";
		
		if(this.isSolo)
			som = "solo";
		else
			som = "Multiplayer";
		
		if(this.difficulty == 0)
			diff = "Easy";
		else if(this.difficulty == 1)
			diff = "Medium";
		else if(this.difficulty == 2)
			diff = "Hard";
		
		this.fm.writeFile(wol+","+som+","+this.scoreP1+","+this.scoreP2+","+diff+",");
	}
	
	public String[] readStats()
	{
		String readfm = this.fm.readFile();
		
		String[] mysplit = readfm.split((","));
		
		return mysplit;
	
	}
}
