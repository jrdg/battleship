import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class LogPanel extends JPanel
{
	private JTextArea jtext;
	private JScrollPane js;
	private ArrayList<LogOperation> logOpList;
	
	public LogPanel()
	{
		this.logOpList = new ArrayList();
		jtext = new JTextArea();
		jtext.setLineWrap(true);
		jtext.setEditable(false);
		jtext.setFocusable(false);
		js = new JScrollPane (jtext, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.setBackground(Color.blue);
		this.setLayout(new BorderLayout());
		this.add(js);
	}
	
	public void addLogOperation(LogOperation logOp)
	{
		this.logOpList.add(logOp);
		updateLog();
	}
	
	public void resetLogOperation()
	{
		this.logOpList.clear();
		this.jtext.setText("");
	}
	
	private void updateLog()
	{
		String allLog = "";
		
		for(int i = 0 ; i <= this.logOpList.size()-1 ; i++)
		{
			allLog += this.logOpList.get(i).toString()+"\n";
		}
		
		this.jtext.setText(allLog);
	}
}
