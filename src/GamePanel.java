import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

public class GamePanel extends JPanel implements KeyListener
{
	private boolean drag;
	private int difficulty;
	private int mode;
	private Player p1;
	private Player p2;
	private JPanel pan1; //panel global
	private JPanel pan2; //panel qui contient les grille de joueur
	private pan21 pan21; //grille du joueur 1
	private pan22 pan22; //grille du joueur 2
	private JPanel pan3; //panel qui contient les bateau a drag n drop
	private StatsPanel sp; //panneau des statistisque tout en haut
	private ActionListener p1Action;
	private ActionListener p2Action;
	private BoatPickerPanel bp;
	private int[][] p1Case;
	private int[][] p2Case;
	private Bateau[] bateauTab;
	private Bateau[] bateauTabOwn;
	private boolean playerTurn;
	private boolean isStart;
	private ArrayList<Integer> botCasePlayed;
	private final int timeBeforeBotPlay = 10;
	private IA IA;
	
	
	public GamePanel(int difficulty , int mode)
	{
		IA = new IA();
		this.addKeyListener(this);
		this.setFocusable(true);
		drag = false;
		this.difficulty = difficulty;
		this.mode = mode;
		this.botCasePlayed = new ArrayList();
		p1= new Player("Player 1");
		p2 = new Player("Player 2");
		this.isStart = false;
		this.playerTurn = true;
		bateauTab = new Bateau[4];
		bateauTabOwn = new Bateau[4];
		p1Case = new int[10][10];
		p2Case = new int[10][10];
		bp = new BoatPickerPanel();
		this.setLayout(new BorderLayout());
		pan1 = new JPanel();
		pan3 = new JPanel();
		pan1.setLayout(new BorderLayout());
		sp = new StatsPanel();
		sp.setPlayerTurn(true);
		pan1.add(sp,BorderLayout.NORTH);
		pan2 = new JPanel();
		pan2.setLayout(new GridLayout(1,2));
		pan21 = new pan21();
		pan21.setBackground(Color.black);
		pan21.setLayout(new GridLayout(10,10));
		pan21.setBorder(BorderFactory.createEmptyBorder(0, 15, 15, 13));
		
		this.bp.getRandom().addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{			
				randomizePositioningPlayer();
				((JButton)arg0.getSource()).setEnabled(false);
			}
			
		});
		
		this.bp.getReady().addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				if(!bp.getRandom().isEnabled())
				{
					 throwStartTimer();
					((JButton)arg0.getSource()).setEnabled(false);
					bp.reset.setEnabled(false);
				}
				else
					JOptionPane.showMessageDialog(null, "You need to put all of your boat on the map","error",JOptionPane.ERROR_MESSAGE);
			}
			
		});
		
		this.bp.getReset().addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(!isStart)
				{
					bateauTabOwn = new Bateau[4];
					
					for(int i = 0 ; i <= p1Case.length-1 ; i++)
					{
						for(int j = 0 ; j <= p1Case.length-1 ; j++)
							p1Case[i][j] = 0;
					}
					
					for(int i = 0 ; i <= 100-1 ; i++)
						pan21.getComponent(i).setBackground(Color.darkGray);
					
					bp.getRandom().setEnabled(true);
				}			
			}
			
		});
		
		
		p1Action = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				boolean touch = false;
	
				if(isStart)
				{
					if(playerTurn)
					{
						((JButton)e.getSource()).setEnabled(false);
						
						int n = Integer.parseInt(((JButton)e.getSource()).getActionCommand());
						
						((JButton)e.getSource()).setBackground(Color.lightGray);
						
						boolean istouch = false;
						
						for(int i = 0 ; i <= bateauTab.length-1 ; i++)
						{	
							if(bateauTab[i].isTouch(n))
							{
								istouch = true;
								bp.getLogPanel().addLogOperation(new LogOperation(true,false,true,n,bateauTab[i]));
								if(!bateauTab[i].isAlive)
								{
									bp.getLogPanel().addLogOperation(new LogOperation(false,true,true,n,bateauTab[i]));
									for(int l = 0 ; l <= bateauTab[i].getList().size()-1 ; l++)
										((JButton)pan22.getComponent(bateauTab[i].getList().get(l))).setBackground(Color.red);
									p1.setScore(p1.getScore()+1);
									p1.setShipDestroyed(p1.getShipDestroyed()+1);
									p2.setOwnShipDestroyed(p2.getOwnShipDestroyed()+1);
									sp.setOwnShipDieP2(p2.getOwnShipDestroyed());
									sp.setScoreP1(p1.getScore());
									sp.setShipKilledP1(p1.getShipDestroyed());
								}
								else
								{
									
									((JButton)e.getSource()).setBackground(Color.orange);
									p1.setScore(p1.getScore()+1);
									sp.setScoreP1(p1.getScore());
								}						
								sp.repaint();
							}	
						}
						
						if(!istouch)
							bp.getLogPanel().addLogOperation(new LogOperation(false,false,true,n,null));
						
						playerTurn = false;
						sp.setPlayerTurn(false);
						sp.repaint();
						
						Thread t = new Thread(new Runnable()
						{
	
							@Override
							public void run()
							{			
								int timer = 3;
								
									while(timer > -1)
									{
										timer--;
										
										synchronized(this)
										{
											try {
												this.wait(timeBeforeBotPlay);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										}
	
									}
									
									boolean alreadyPlay = false;
									int randcase;
									
									
									//ces dans cette boucle quon decide ou le bot jouera (randcase)
									do
									{
										randcase = (int) Math.round(Math.random() * 99);
										
										if(difficulty == 2)
										{
											if(IA.isContainingBoat())
												randcase = IA.HardLvl();
										}

										if(botCasePlayed.contains(randcase))
											alreadyPlay = true;
										else
										{
											alreadyPlay = false;
											//System.out.println("le bot ne pourra plus jouer a la case: "+randcase);
											botCasePlayed.add(randcase);
										}									
									}while(alreadyPlay || randcase > 99 || randcase < 0);
									
									if(randcase > -1 && randcase < 100)
										pan21.getComponent(randcase).setBackground(Color.lightGray);
									
									boolean istouch = false;
									
									for(int i = 0 ; i <= bateauTabOwn.length-1 ; i++)
									{	
										//System.out.println("null pointeur exeption case : "+randcase);
										if(bateauTabOwn[i].isTouch(randcase))
										{
											
											///////////////////////////
											////////////////////////////
											/////////////////////////////
											
											if(difficulty == 2)
											{
												boolean alreadyIn = false;
												
												
												//boucle qui teste si le bateau toucher a deja ete donne au IA
												for(int h = 0 ; h <= IA.getBateauList().size()-1 ; h++)
												{
													if(bateauTabOwn[i] instanceof Destroyer)
													{
														if(IA.getBateauList().get(h) instanceof Destroyer)
														{
															//System.out.println("already touch a destroyer");
															alreadyIn = true;
														}
													}
													else if(bateauTabOwn[i] instanceof SousMarin)
													{
														if(IA.getBateauList().get(h) instanceof SousMarin)
														{
															//System.out.println("already touch a sousmarin");
															alreadyIn = true;
														}
													}
													else if(bateauTabOwn[i] instanceof PorteAvion)
													{
														if(IA.getBateauList().get(h) instanceof PorteAvion)
														{
															//System.out.println("already touch a porteavion");
															alreadyIn = true;
														}
													}
													else if(bateauTabOwn[i] instanceof BateauPatrouille)
													{
														if(IA.getBateauList().get(h) instanceof BateauPatrouille)
														{
														//	System.out.println("already touch a BateauPatrouille");
															alreadyIn = true;
														}
													}
												}
												
												//si linstance du bateau toucher na pas ete ajouter au IA on lajoute ainsi que sa startPos
												if(!alreadyIn)
												{
													IA.addBateau(bateauTabOwn[i]);
													IA.addStartPos(randcase);
													//System.out.println("bateau ajouter");
													//System.out.println("Case : "+randcase);
												}	
											}										
											////////////////////////////
											///////////////////////////
											///////////////////////////
											
											istouch = true;
											bp.getLogPanel().addLogOperation(new LogOperation(true,false,false,randcase,bateauTab[i]));
									
											
											if(!bateauTabOwn[i].isAlive)
											{
												bp.getLogPanel().addLogOperation(new LogOperation(false,true,false,randcase,bateauTab[i]));

												for(int l = 0 ; l <= bateauTabOwn[i].getList().size()-1 ; l++)
													pan21.getComponent(bateauTabOwn[i].getList().get(l)).setBackground(Color.red);
												p2.setScore(p2.getScore()+1);
												p2.setShipDestroyed(p2.getShipDestroyed()+1);
												p1.setOwnShipDestroyed(p1.getOwnShipDestroyed()+1);
												sp.setOwnShipDieP1(p1.getOwnShipDestroyed());
												sp.setScoreP2(p2.getScore());
												sp.setShipKilledP2(p2.getShipDestroyed());
											}
											else
											{
												pan21.getComponent(randcase).setBackground(Color.orange);
												p2.setScore(p2.getScore()+1);
												sp.setScoreP2(p2.getScore());
											}
												
										}							
									}
									
									if(!istouch)
										bp.getLogPanel().addLogOperation(new LogOperation(false,false,false,randcase,null));
									
									playerTurn = true;
									sp.setPlayerTurn(true);
									sp.repaint();
									
									if(p1.getShipDestroyed() == 4 && p2.getShipDestroyed() < 4)
									{
										sp.getMinObject().stop();
										JOptionPane.showMessageDialog(null,"Player 1 win","Game is finish",JOptionPane.INFORMATION_MESSAGE);
										
										FinalStats fs = new FinalStats("win",true,sp.getScoreP1(),sp.getScoreP2(),difficulty);
										fs.addStats();
										
										int g = JOptionPane.showConfirmDialog(null, "Do you want to make another game ?","Restart",JOptionPane.YES_NO_OPTION);
										
										if(g == JOptionPane.YES_OPTION)
											resetGame();
										else
										{
											((JFrame)getRootPane().getParent()).dispose();
											new Menu(difficulty,mode);
										}

									}
									if(p2.getShipDestroyed() == 4 && p1.getShipDestroyed() < 4)
									{
										sp.getMinObject().stop();
										JOptionPane.showMessageDialog(null,"Player 2 win","Game is finish",JOptionPane.INFORMATION_MESSAGE);
										
										FinalStats fs = new FinalStats("loose",true,sp.getScoreP1(),sp.getScoreP2(),difficulty);
										fs.addStats();
										
										int g = JOptionPane.showConfirmDialog(null, "Do you want to make another game ?","Restart",JOptionPane.YES_NO_OPTION);
										
										if(g == JOptionPane.YES_OPTION)
											resetGame();							
										else
										{
											((JFrame)getRootPane().getParent()).dispose();
											new Menu(difficulty,mode);
										}
									}
									if(p2.getShipDestroyed() == 4 && p1.getShipDestroyed() == 4)
									{
										sp.getMinObject().stop();
										JOptionPane.showMessageDialog(null,"DRAW","Game is finish",JOptionPane.INFORMATION_MESSAGE);
										
										FinalStats fs = new FinalStats("draw",true,sp.getScoreP1(),sp.getScoreP2(),difficulty);
										fs.addStats();
										
										int g = JOptionPane.showConfirmDialog(null, "Do you want to make another game ?","Restart",JOptionPane.YES_NO_OPTION);
										
										if(g == JOptionPane.YES_OPTION)
											resetGame();
										else
										{
											((JFrame)getRootPane().getParent()).dispose();
											new Menu(difficulty,mode);
										}
									}
							}		
						});
						
						t.start();			
					}
				}

			}		
		};
		
		for(int i = 0 ; i <= 100-1 ; i++)
		{
			pan21.add(new JButton());
			pan21.getComponent(i).setBackground(Color.DARK_GRAY);
			((JComponent) pan21.getComponent(i)).setBorder(new LineBorder(Color.white));
		}
			
		
		pan22 = new pan22();
		pan22.setBackground(Color.black);
		pan22.setLayout(new GridLayout(10,10));
		pan22.setBorder(BorderFactory.createEmptyBorder(0, 13, 15, 15));
		
		for(int i = 0 ; i <= 100-1 ; i++)
		{
			pan22.add(new JButton());
			((JButton)pan22.getComponent(i)).setActionCommand(Integer.toString(i));
			((JButton)pan22.getComponent(i)).addActionListener(p1Action);
			pan22.getComponent(i).setFocusable(false);
			pan22.getComponent(i).setBackground(Color.DARK_GRAY);
			((JComponent) pan22.getComponent(i)).setBorder(new LineBorder(Color.white));
		}
			
	
		pan2.add(pan21);
		pan2.add(pan22);
		pan1.add(pan2);
		pan1.setBackground(Color.black);
		pan1.add(bp,BorderLayout.SOUTH);
		pan1.setPreferredSize(new Dimension(0,0));
		this.add(pan1);
		this.randomizePositioningBot();
	}
	
	public StatsPanel getSp()
	{
		return this.sp;
	}
	
	public boolean getIsStart()
	{
		return this.isStart;
	}
	
	public void resetGame()
	{		
		this.IA.IAreset();
		
		//on reset tous le log
		this.bp.getLogPanel().resetLogOperation();
		
		//reset les tableau de bateau
		bateauTab = new Bateau[4];
		bateauTabOwn = new Bateau[4];
		
		//reset les tableau multidiemensionell
		p1Case = new int[10][10];
		p2Case = new int[10][10];
		
		//reset le temps
		sp.getMinObject().stop();
		sp.getMinObject().resetTime();
		
		//remet tous les bouton des 2 grille a enabled
		for(int i = 0 ; i <= 100-1 ; i++)
		{
			pan22.getComponent(i).setEnabled(true);
			pan21.getComponent(i).setEnabled(true);
		}
		
		
		//remet la couleurd e tous les boutons a darkgray
		for(int i = 0 ; i <= 100-1 ; i++)
		{
			pan21.getComponent(i).setBackground(Color.darkGray);
			pan22.getComponent(i).setBackground(Color.darkGray);
		}
		
		//remet le score a 0
		p1.setScore(0);
		p1.setOwnShipDestroyed(0);
		p1.setShipDestroyed(0);
		p2.setScore(0);
		p2.setOwnShipDestroyed(0);
		p2.setShipDestroyed(0);							
		sp.setScoreP1(p1.getScore());
		sp.setShipKilledP1(p1.getShipDestroyed());
		sp.setOwnShipDieP1(p1.getOwnShipDestroyed());
		sp.setScoreP2(p2.getScore());
		sp.setShipKilledP2(p2.getShipDestroyed());
		sp.setOwnShipDieP2(p2.getOwnShipDestroyed());
		
		sp.setTime(-1);
		
		sp.repaint();
		
		//remet la parti a pas start
		isStart = false;
		
		//remove tous les case deja jouer par lee bot
		this.botCasePlayed.clear();
		
		//renabled tous les bouton de demarrage
		bp.getRandom().setEnabled(true);
		bp.getReady().setEnabled(true);
		bp.getReset().setEnabled(true);
		
		//on genere des case aleatoire pour le bot
		randomizePositioningBot();	
	}
	
	public void throwStartTimer()
	{
		Thread tstart = new Thread(new Runnable()
		{
			@Override
			public void run() 
			{
				int timer = 3;
				
				while(timer > -1)
				{		
					if(timer == 0)
					{
						isStart = true;
						sp.getMinObject().startMinuterie(sp,Chrono.Up);
					}
						
					
					sp.setTime(timer);
					timer--;
					
					sp.repaint();
					synchronized(this)
					{	
						try {
							this.wait(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}		
		});
		
		tstart.start();
	}
	
	public void randomizePositioningBot()
	{
		boolean isEmpty = true;
		//Bateau b;
		Direction direction;
		
		
		for(int k = 0 ; k <= 4-1 ; k++)
		{
			do
			{
				isEmpty = true;
				int x = (int) Math.round(Math.random() * 9);
				int y = (int) Math.round(Math.random() * 9);
				int alignement = (int) Math.round(Math.random() * 1);
				
				if(alignement == 0)
					direction = Direction.Vertical;
				else
					direction = Direction.Horizontal;
				
				if(k == 0)		
					bateauTab[0] = new Destroyer(3,direction,x,y);
				else if (k == 1)
					bateauTab[1] = new PorteAvion(4,direction,x,y);
				else if(k == 2)
					bateauTab[2] = new SousMarin(3,direction,x,y);
				else
					bateauTab[3] = new BateauPatrouille(2,direction,x,y);
				
				
				
				if(bateauTab[k].direction == Direction.Vertical)
				{
					if(bateauTab[k].getNbCase() == 2)
					{
						if(bateauTab[k].getY() >= 9)
							isEmpty = false;
					}
					else if(bateauTab[k].getNbCase() == 3)
					{
						if(bateauTab[k].getY() >= 8)
							isEmpty = false;
					}
					else if(bateauTab[k].getNbCase() == 4)
					{
						if(bateauTab[k].getY() >= 7)
							isEmpty = false;
					}
				}
				else
				{
					if(bateauTab[k].getNbCase() == 2)
					{
						if(bateauTab[k].getX() >= 9)
							isEmpty = false;
					}
					else if(bateauTab[k].getNbCase() == 3)
					{
						if(bateauTab[k].getX() >= 8)
							isEmpty = false;							
					}
					else if(bateauTab[k].getNbCase() == 4)
					{
						if(bateauTab[k].getX() >= 7)
							isEmpty = false;
					}
				}
				
				//si la tete es trop proche du bar on ne fais pas se qui es plus bas on passe a la prochaine iteration
				if(!isEmpty)
					continue;
				
				for(int i = 0 ; i <= bateauTab[k].getNbCase()-1 ; i++)
				{
					if(bateauTab[k].direction == Direction.Vertical)
					{
						if(p2Case[y+i][x] == 1 || p2Case[y+i][x] == 2 || p2Case[y+i][x] == 3 || p2Case[y+i][x] == 4)
							isEmpty = false;	
					}
					else
						if(p2Case[y][x+i] == 1 || p2Case[y][x+i] == 2 || p2Case[y][x+i] == 3 || p2Case[y][x+i] == 4)
							isEmpty = false;			
				}
				
				if(isEmpty)
				{
					isEmpty = true;
					
					for(int i = 0 ; i <= bateauTab[k].getNbCase()-1 ; i++)
					{
						if(bateauTab[k].direction == Direction.Vertical)
							p2Case[y+i][x] = k+1;
						else
							p2Case[y][x+i] = k+1;
					}	
					
					///////////////////DESSIN LES BOUTON SUR LE PANEL//////////////////////
					String caseNber = Integer.toString(bateauTab[k].getX()) + Integer.toString(bateauTab[k].getY());
					
					int caseNberToInt = Integer.parseInt(caseNber);
					
					//la tete du bateau
					//pan22.getComponent(caseNberToInt).setBackground(Color.green);
					bateauTab[k].addPosition(caseNberToInt);
					
					//les case restante du bateau
					if(bateauTab[k].getDirection() == Direction.Vertical)
					{
						for(int i = 0 ; i <= bateauTab[k].getNbCase()-2 ; i++)
						{
							caseNberToInt += 1;
							//pan22.getComponent(caseNberToInt).setBackground(Color.green);
							bateauTab[k].addPosition(caseNberToInt);
						}
					}
					else if(bateauTab[k].getDirection() == Direction.Horizontal)
					{
						for(int i = 0 ; i <= bateauTab[k].getNbCase()-2 ; i++)
						{
							caseNberToInt += 10;
							//pan22.getComponent(caseNberToInt).setBackground(Color.green);
							bateauTab[k].addPosition(caseNberToInt);
						}
					}
					//////////////////////////////////////////////////////////////////////
					
				}
				
			}while(!isEmpty);
		}
	}
	
	public void randomizePositioningPlayer()
	{
		boolean isEmpty = true;
		Direction direction;
		
		
		for(int k = 0 ; k <= 4-1 ; k++)
		{
			do
			{
				isEmpty = true;
				int x = (int) Math.round(Math.random() * 9);
				int y = (int) Math.round(Math.random() * 9);
				int alignement = (int) Math.round(Math.random() * 1);
				
				if(alignement == 0)
					direction = Direction.Vertical;
				else
					direction = Direction.Horizontal;
				
				if(k == 0)		
					bateauTabOwn[0] = new Destroyer(3,direction,x,y);
				else if (k == 1)
					bateauTabOwn[1] = new PorteAvion(4,direction,x,y);
				else if(k == 2)
					bateauTabOwn[2] = new SousMarin(3,direction,x,y);
				else
					bateauTabOwn[3] = new BateauPatrouille(2,direction,x,y);
				
				
				
				if(bateauTabOwn[k].direction == Direction.Vertical)
				{
					if(bateauTabOwn[k].getNbCase() == 2)
					{
						if(bateauTabOwn[k].getY() >= 9)
							isEmpty = false;
					}
					else if(bateauTabOwn[k].getNbCase() == 3)
					{
						if(bateauTabOwn[k].getY() >= 8)
							isEmpty = false;
					}
					else if(bateauTabOwn[k].getNbCase() == 4)
					{
						if(bateauTabOwn[k].getY() >= 7)
							isEmpty = false;
					}
				}
				else
				{
					if(bateauTabOwn[k].getNbCase() == 2)
					{
						if(bateauTabOwn[k].getX() >= 9)
							isEmpty = false;
					}
					else if(bateauTabOwn[k].getNbCase() == 3)
					{
						if(bateauTabOwn[k].getX() >= 8)
							isEmpty = false;							
					}
					else if(bateauTabOwn[k].getNbCase() == 4)
					{
						if(bateauTabOwn[k].getX() >= 7)
							isEmpty = false;
					}
				}
				
				//si la tete es trop proche du bar on ne fais pas se qui es plus bas on passe a la prochaine iteration
				if(!isEmpty)
					continue;
				
				for(int i = 0 ; i <= bateauTabOwn[k].getNbCase()-1 ; i++)
				{
					if(bateauTabOwn[k].direction == Direction.Vertical)
					{
						if(p1Case[y+i][x] == 1 || p1Case[y+i][x] == 2 || p1Case[y+i][x] == 3 || p1Case[y+i][x] == 4)
							isEmpty = false;	
					}
					else
						if(p1Case[y][x+i] == 1 || p1Case[y][x+i] == 2 || p1Case[y][x+i] == 3 || p1Case[y][x+i] == 4)
							isEmpty = false;			
				}
				
				if(isEmpty)
				{
					isEmpty = true;
					
					for(int i = 0 ; i <= bateauTabOwn[k].getNbCase()-1 ; i++)
					{
						if(bateauTabOwn[k].direction == Direction.Vertical)
							p1Case[y+i][x] = k+1;
						else
							p1Case[y][x+i] = k+1;
					}	
					
					
					///////////////////DESSIN LES BOUTON SUR LE PANEL//////////////////////
					String caseNber = Integer.toString(bateauTabOwn[k].getX()) + Integer.toString(bateauTabOwn[k].getY());
					
					int caseNberToInt = Integer.parseInt(caseNber);
					
					//la tete du bateau
					pan21.getComponent(caseNberToInt).setBackground(Color.green);
					bateauTabOwn[k].addPosition(caseNberToInt);
					
					//les case restante du bateau
					if(bateauTabOwn[k].getDirection() == Direction.Vertical)
					{
						for(int i = 0 ; i <= bateauTabOwn[k].getNbCase()-2 ; i++)
						{
							caseNberToInt += 1;
							pan21.getComponent(caseNberToInt).setBackground(Color.green);
							bateauTabOwn[k].addPosition(caseNberToInt);
						}
					}
					else if(bateauTabOwn[k].getDirection() == Direction.Horizontal)
					{
						for(int i = 0 ; i <= bateauTabOwn[k].getNbCase()-2 ; i++)
						{
							caseNberToInt += 10;
							pan21.getComponent(caseNberToInt).setBackground(Color.green);
							bateauTabOwn[k].addPosition(caseNberToInt);
						}
					}

					
					//////////////////////////////////////////////////////////////////////
				}
				
			}while(!isEmpty);
		}
	}
	
	public void cheat()
	{
		for(int i = 0 ; i <= bateauTab.length-1 ; i++)
		{		
			for(int j = 0 ; j <= bateauTab[i].getList().size()-1 ; j++)
			{
				if(pan22.getComponent(this.bateauTab[i].getList().get(j)).getBackground() == Color.darkGray)
				{
					pan22.getComponent(this.bateauTab[i].getList().get(j)).setBackground(Color.green);
				}				
				else if(pan22.getComponent(this.bateauTab[i].getList().get(j)).getBackground() == Color.green)
					pan22.getComponent(this.bateauTab[i].getList().get(j)).setBackground(Color.darkGray);
				pan22.repaint();
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		   int key = e.getKeyCode();
		    if (key == KeyEvent.VK_LEFT) 
		    {
		    	if(this.isStart)
		    		cheat();	    		
		    }
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}

