import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class HofFrame extends JFrame
{
	private JPanel hofPanel;
	private JTable tableau;
	private JScrollPane js;
	private int totalWin;
	private int totalLoss;
	private int totalScore;
	private int totalScoreE;
	private JLabel win;
	private JLabel lossl;
	private JLabel score;
	private JLabel scoreE;
	
	public HofFrame()
	{
		
		this.setTitle("Battleship");
		this.setSize(800, 600);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		
		JPanel scorePanel = new JPanel();
		scorePanel.setLayout(new GridLayout(1,4));
		lossl = new JLabel("Loss : 0");
		win = new JLabel("total win: "+this.totalWin);
		score = new JLabel("total score: 0");
		scoreE = new JLabel("total Enemy score: 0");
		win.setHorizontalAlignment(JLabel.CENTER);
		scoreE.setHorizontalAlignment(JLabel.CENTER);
		lossl.setHorizontalAlignment(JLabel.CENTER);
		score.setHorizontalAlignment(JLabel.CENTER);
		String[] title = {"win/loss","solo/multi","score player","score enemy","Difficulty"};
		FinalStats f = new FinalStats();
		String[] stab = f.readStats();
		int nb = (stab.length/5)+1; //vaut 2
		Object[][] userInfo = new Object[nb-1][5]; //tableau avec 
		
		
		//boucle du y
		int rendu = 0;
		String confirm = "";
		for(int i = 0 ; i <= nb-2; i++)
		{
			//boucle du x
			for(int j = 0 ; j <= 5-1 ; j++)
			{			
				userInfo[i][j] = stab[rendu];
				rendu++;		
			}
		}	
		
		
		hofPanel = new JPanel();
		tableau = new JTable(userInfo,title);
		js = new JScrollPane (tableau, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		this.updateInfo();
			
		tableau.setEnabled(false);
		tableau.getTableHeader().setReorderingAllowed(false);
		tableau.getTableHeader().setResizingAllowed(false);
		this.hofPanel.setLayout(new BorderLayout());
		scorePanel.add(win);
		scorePanel.add(lossl);
		scorePanel.add(scoreE);
		scorePanel.add(score);
		this.hofPanel.add(scorePanel,BorderLayout.NORTH);
		this.hofPanel.add(js);
		this.setContentPane(hofPanel);
		this.setVisible(true);
	}
	
	public void updateInfo()
	{
		//update loss and win
		for(int i = 0 ; i <= tableau.getRowCount()-1; i++)
		{
			String confirme = (String) tableau.getValueAt(i, 0);
			//System.out.println(confirme);
			if(confirme.trim().equals("win"))
				this.totalWin++;
			else if(confirme.trim().equals("loss"))
				this.totalLoss++;
		}
		
		win.setText("Total win : "+this.totalWin);
		lossl.setText("Total loss : "+this.totalLoss);
		
		//update score Player
		for(int i = 0 ; i <= tableau.getRowCount()-1; i++)
		{
			this.totalScore += Integer.parseInt((String)tableau.getValueAt(i, 2));
		}
		
		this.score.setText("Total score : "+this.totalScore);
		
		//update score enemy
		for(int i = 0 ; i <= tableau.getRowCount()-1; i++)
		{
			this.totalScoreE += Integer.parseInt((String)tableau.getValueAt(i,3));
		}
		
		this.scoreE.setText("Total enemy score : "+this.totalScoreE);
	}
}
