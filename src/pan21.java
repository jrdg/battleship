import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class pan21 extends JPanel
{
	public pan21()
	{
		this.setBackground(Color.black);
	}
	
	public void paintComponent(Graphics g)
	{
		g.setColor(Color.black);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.white);
		g.setFont(new Font("Arial", Font.PLAIN, 15));
		
		String alpha = "ABCDEFGHIJ";
		
		int n = 25;
		for(int i = 0 ; i <= alpha.toCharArray().length-1 ; i++)
		{
			g.drawString(Character.toString(alpha.toCharArray()[i]), 5,n );
			n += 45 - i + 1;
		}
	}
}
