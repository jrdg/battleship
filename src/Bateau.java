import java.util.ArrayList;

abstract public class Bateau 
{
	protected int nbCase;
	protected boolean isAlive;
	protected int life;
	protected Direction direction;
	protected int x;
	protected int y;
	protected ArrayList posTab;
	public int count;
	public Bateau(int nbCase , Direction direction , int x , int y)
	{
		this.count = 0;
		posTab = new ArrayList<Integer>();
		this.life = nbCase;
		this.x = x;
		this.y = y;
		this.nbCase = nbCase;
		this.direction = direction;
		this.isAlive = true;
	}
	
	public void addPosition(int pos)
	{
		this.posTab.add(pos);
	}
	
	public ArrayList<Integer> getList()
	{
		return this.posTab;
	}
	
	public void afficher()
	{
		for(int i = 0 ; i <= this.posTab.size()-1 ; i++)
			System.out.println("Position : "+posTab.get(i));
	}
	
	public boolean isTouch(int caseNumber)
	{
		if(this.posTab.contains(caseNumber))
		{
			this.life--;
			
			if(this.life == 0)
				this.isAlive = false;
			
			return true;
		}			
		else
			return false;
	}
	
	public boolean isTouchOnly(int casenumber)
	{
		if(this.posTab.contains(casenumber))
		{		
			return true;
		}			
		else
			return false;
	}
	
	public boolean checkTouch(int casenumber)
	{
		if(this.posTab.contains(casenumber))
		{		
			return true;
		}			
		else
			return false;
	}

	public int getNbCase() {
		return nbCase;
	}

	public void setNbCase(int nbCase) {
		this.nbCase = nbCase;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public boolean getIsAlive()
	{
		return this.isAlive;
	}
	
	public int getLife()
	{
		return this.life;
	}
}
